FROM openjdk:8-jdk-alpine
EXPOSE 8080
COPY target/*.jar app.jar
ENTRYPOINT ["java","-jar","/hive-1.0-SNAPSHOT.jar"]
