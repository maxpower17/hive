package app.repository;

import com.mongodb.MongoClient;
import dev.morphia.Datastore;
import dev.morphia.Morphia;

public abstract class IRepository {

    protected Datastore datastore;

    public IRepository() {
        MongoClient mongo = new MongoClient();
        Morphia morphia = new Morphia();
        datastore = morphia.createDatastore(mongo, "hive");
    }

}
