package app.repository;

import app.models.dto.BeeDTO;
import app.models.orm.Bee;
import dev.morphia.query.Query;
import dev.morphia.query.UpdateOperations;

import javax.inject.Singleton;
import java.util.List;

@Singleton
public class BeeRepository extends IRepository {

    public void save(Bee bee) {
        datastore.save(bee);
    }

    public void save(List<Bee> bees) {
        datastore.save(bees);
    }

    public List<Bee> getAliveBeesByHiveId(Integer hiveId) {
        return datastore.createQuery(Bee.class).field("hiveId").equal(hiveId)
                .field("isAlive").equal(true).asList();
    }

    public void update(BeeDTO beeDTO) {
        Query<Bee> query = datastore.createQuery(Bee.class).field("id").equal(beeDTO.getId());
        UpdateOperations<Bee> ops = datastore.createUpdateOperations(Bee.class).set("isBringingNectar", true);

        datastore.update(query, ops);
    }

    public List<Bee> getBringingNectarBeesByHiveId(Integer hiveId) {
        return datastore.createQuery(Bee.class).field("hiveId").equal(hiveId)
                .field("isBringingNectar").equal(true).asList();
    }

}
