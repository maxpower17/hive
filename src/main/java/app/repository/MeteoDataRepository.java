package app.repository;

import app.models.orm.MeteoData;

import javax.inject.Singleton;

@Singleton
public class MeteoDataRepository extends IRepository {

    public void save(MeteoData meteoData) {
        datastore.save(meteoData);
    }


}
