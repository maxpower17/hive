package app.repository;

import app.models.orm.Bribe;

import javax.inject.Singleton;

@Singleton
public class BribeRepository extends IRepository {

    public void save(Bribe bee) {
        datastore.save(bee);
    }

}
