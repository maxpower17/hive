package app.repository;

import app.models.orm.Hive;

import javax.inject.Singleton;

@Singleton
public class HiveRepository extends IRepository {

    public void save(Hive hive) {
        datastore.save(hive);
    }
}
