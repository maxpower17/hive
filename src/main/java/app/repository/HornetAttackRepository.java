package app.repository;

import app.models.orm.HornetAttack;

import javax.inject.Singleton;

@Singleton
public class HornetAttackRepository extends IRepository {

    public void save(HornetAttack attack) {
        datastore.save(attack);
    }
}
