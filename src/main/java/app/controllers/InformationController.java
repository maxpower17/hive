package app.controllers;

import app.service.InformationService;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.Produces;

import javax.inject.Inject;

@Controller("/information")
public class InformationController {

    @Inject
    InformationService informationService;

    @Get("/bees")
    @Produces(MediaType.APPLICATION_JSON)
    public String bees(@PathVariable Integer hiveId) {
        Integer res = informationService.numberOfBees(hiveId);
        return res.toString();
    }

    @Get("/bees-collectors")
    @Produces(MediaType.APPLICATION_JSON)
    public String beesCollectors(@PathVariable Integer hiveId) {
        Integer res = informationService.numberOfBees(hiveId);
        return res.toString();
    }
}
