package app.controllers;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Produces;

@Controller("/analytics")
public class AnalyticsController {

    @Get("/")
    @Produces(MediaType.APPLICATION_JSON)
    public String index() {
        return "analytics";
    }

}
