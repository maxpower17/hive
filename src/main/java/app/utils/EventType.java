package app.utils;

public class EventType {
    public static final String NEW_HIVE = "hive";
    public static final String SEARCH_NECTAR = "search";
    public static final String BRING_NECTAR = "bring";
    public static final String HORNET_ATTACK = "hornet";
    public static final String BEE_BIRTH = "birth";
}