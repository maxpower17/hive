package app.utils;

import app.models.active.AddHiveActivity;
import app.models.active.BringingBribeActivity;
import app.models.active.HornetAttackActivity;
import app.models.active.SearchNectarActivity;
import app.service.EventService;
import com.google.gson.Gson;
import io.micronaut.configuration.rabbitmq.annotation.Queue;
import io.micronaut.configuration.rabbitmq.annotation.RabbitListener;
import io.micronaut.http.annotation.Header;

import javax.inject.Inject;

@RabbitListener
public class EventConsumer {

    private Gson gson = new Gson();

    @Inject
    private EventService service;

    @Queue("activity")
    public void receive(byte[] data, @Header("type") String type) {

        if (EventType.NEW_HIVE.equals(type)) {
            AddHiveActivity activity = gson.fromJson(new String(data), AddHiveActivity.class);

            service.handleNewHiveEvent(activity);
        } else if (EventType.BEE_BIRTH.equals(type)) {

        } else if (EventType.BRING_NECTAR.equals(type)) {
            BringingBribeActivity activity = gson.fromJson(new String(data), BringingBribeActivity.class);

            service.handleBringNectarEvent(activity);
        } else if (EventType.HORNET_ATTACK.equals(type)) {
            HornetAttackActivity activity = gson.fromJson(new String(data), HornetAttackActivity.class);

            service.handleHornetAttackEvent(activity);
        } else if (EventType.SEARCH_NECTAR.equals(type)) {
            SearchNectarActivity activity = gson.fromJson(new String(data), SearchNectarActivity.class);

            service.handleSearchNectarEvent(activity);
        }

    }

}
