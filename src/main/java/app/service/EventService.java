package app.service;

import app.models.active.AddHiveActivity;
import app.models.active.BringingBribeActivity;
import app.models.active.HornetAttackActivity;
import app.models.active.SearchNectarActivity;
import app.models.dto.BeeDTO;
import app.models.orm.*;
import app.repository.*;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Date;
import java.util.List;

@Singleton
public class EventService {

    @Inject
    HiveRepository hiveRepository;

    @Inject
    BeeRepository beeRepository;

    @Inject
    MeteoDataRepository meteoDataRepository;

    @Inject
    BribeRepository bribeRepository;

    @Inject
    HornetAttackRepository hornetAttackRepository;

    public void handleNewHiveEvent(AddHiveActivity activity) {
        hiveRepository.save(new Hive(activity.hive.getId()));

        List<BeeDTO> beesIds = activity.bees;

        for (BeeDTO bee: beesIds) {
            beeRepository.save(new Bee(bee.getId(), activity.hive.getId(),
                    true, false));
        }

    }

    public void handleBeeBirthEvent() {}

    public void handleBringNectarEvent(BringingBribeActivity activity) {
        bribeRepository.save(new Bribe(activity.bribeDTO.getId(),
                activity.bribeDTO.getSize(),
                activity.hive.getId(),
                activity.bee.getId()));
    }

    public void handleHornetAttackEvent(HornetAttackActivity activity) {
        hornetAttackRepository.save(new HornetAttack(activity.hornetSize, activity.hive.getId()));
    }

    public void handleSearchNectarEvent(SearchNectarActivity activity) {
        beeRepository.update(activity.bee);

        meteoDataRepository.save(new MeteoData(activity.meteoData.getId(),
                activity.meteoData.getTemperature(),
                activity.meteoData.getHumidity(),
                activity.meteoData.getPressure(),
                new Date()));
    }

}
