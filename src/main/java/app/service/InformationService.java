package app.service;

import app.repository.BeeRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class InformationService {

    @Inject
    BeeRepository beeRepository;

    public Integer numberOfBees(Integer hiveId) {
        return beeRepository.getAliveBeesByHiveId(hiveId).size();
    }

    public Integer numberOfCollectorBees(Integer hiveId) {
        return beeRepository.getBringingNectarBeesByHiveId(hiveId).size();
    }
}
