package app.models.active;

import app.models.dto.BeeDTO;
import app.models.dto.HiveDTO;

import java.util.List;

public class AddHiveActivity {
    public HiveDTO hive;
    public List<BeeDTO> bees;
}
