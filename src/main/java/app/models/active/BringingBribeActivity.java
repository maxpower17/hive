package app.models.active;

import app.models.dto.BeeDTO;
import app.models.dto.BribeDTO;
import app.models.dto.HiveDTO;
import app.models.dto.MeteoDataDTO;

public class BringingBribeActivity {
    public HiveDTO hive;
    public BeeDTO bee;
    public MeteoDataDTO meteoData;
    public BribeDTO bribeDTO;
}
