package app.models.dto;

import lombok.Data;

@Data
public class HornetAttackDTO {
    private Integer hornetSize;
}
