package app.models.dto;

import lombok.Data;

@Data
public class MeteoDataDTO {
    private Integer id;
    private Integer temperature;
    private Integer humidity;
    private Integer pressure;
}
