package app.models.dto;

import lombok.Data;

@Data
public class BribeDTO {
    private Integer id;
    private Integer size;
}
