package app.models.orm;

import dev.morphia.annotations.Id;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class MeteoData {

    @Id
    private Integer id;

    private Integer temperature;

    private Integer humidity;

    private Integer pressure;

    private Date date;
}
