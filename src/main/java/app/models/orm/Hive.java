package app.models.orm;

import dev.morphia.annotations.Id;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Hive {

    @Id
    private Integer id;

}
