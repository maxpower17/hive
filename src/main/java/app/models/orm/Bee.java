package app.models.orm;

import dev.morphia.annotations.Id;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Bee {
    @Id
    private Integer id;

    private Integer hiveId;

    private Boolean isAlive;

    private Boolean isBringingNectar;

}
