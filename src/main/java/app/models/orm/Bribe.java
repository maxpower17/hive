package app.models.orm;

import dev.morphia.annotations.Id;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Bribe {
    @Id
    private Integer id;

    private Integer size;

    private Integer meteoDataId;

    private Integer beeId;
}
