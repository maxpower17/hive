package app.models.orm;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class HornetAttack {

    private Integer hornetSize;
    private Integer hiveId;

}

